﻿using System.Collections.Generic;

namespace Nav_Solution.DataAccess.Models
{
    public class Result
    {
        public bool Success { get; set; }
        public string Message { get; set; }
        public int ErrorCode { get; set; }
        public Result()
        {
            Success = false;
            Message = string.Empty;
            ErrorCode = 500;
        }
    }

    public class GetResult<TEntity> : Result where TEntity : class, new()
    {
        public IEnumerable<TEntity> Entities { get; set; }
    }
}