﻿using Nav_Solution.DataAccess.Models;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace Nav_Solution.DataAccess.Repositories
{
    public interface IRepository<TEntity> where TEntity : class, new()
    {
        #region Get        
        /// <summary>
        /// A Generic synchronous method for selecting many entities with an optional filter parameter (where)
        /// </summary>
        /// <param name="where"></param>
        /// <param name="successMessage"></param>
        /// <param name="navigationProperties"></param>
        /// <returns></returns>
        GetResult<TEntity> Get(Expression<Func<TEntity, bool>> where, string successMessage,
            params Expression<Func<TEntity, object>>[] navigationProperties);

        /// <summary>
        /// A Generic asynchronous method for selecting many entities with an optional filter parameter (where)
        /// </summary>
        /// <param name="where"></param>
        /// <param name="successMessage"></param>
        /// <param name="navigationProperties"></param>
        /// <returns>If where is presented it will return many/one entities, otherwise all entities</returns>
        Task<GetResult<TEntity>> GetAsync(Expression<Func<TEntity, bool>> where, string successMessage,
            params Expression<Func<TEntity, object>>[] navigationProperties);

        /// <summary>
        /// A Generic synchronous method for executing stored procedure with optional parameters
        /// </summary>
        /// <param name="successMessage"></param>
        /// <param name="procedureName"></param>
        /// <param name="parameters"></param>
        /// <returns></returns>
        GetResult<TEntity> ExecuteProcedure(string successMessage, string procedureName, params object[] parameters);

        /// <summary>
        /// A Generic asynchronous method for executing stored procedure with optional parameters
        /// </summary>
        /// <param name="successMessage"></param>
        /// <param name="procedureName"></param>
        /// <param name="parameters"></param>
        /// <returns></returns>
        Task<GetResult<TEntity>> ExecuteProcedureAsync(string successMessage, string procedureName, params object[] parameters);
        #endregion Get

        #region Create        
        /// <summary>
        /// A Generic synchronous method for inserting an entity
        /// </summary>
        /// <param name="entity"></param>
        /// <param name="successMessage"></param>
        /// <returns></returns>
        Result AddOne(TEntity entity, string successMessage);

        /// <summary>
        /// A Generic asynchronous method for inserting an entity
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        Task<Result> AddOneAsync(TEntity entity, string successMessage);
        #endregion Create

        #region Update        
        /// <summary>
        /// A Generic synchronous method for updating many entities
        /// </summary>
        /// <param name="entities"></param>
        /// <param name="successMessage"></param>
        /// <returns></returns>
        Result UpdateMany(IEnumerable<TEntity> entities, string successMessage);

        /// <summary>
        /// A Generic asynchronous method for updating many entities
        /// </summary>
        /// <param name="entities"></param>
        /// <returns></returns>
        Task<Result> UpdateManyAsync(IEnumerable<TEntity> entities, string successMessage);
        #endregion Update

        #region Delete                
        /// <summary>
        /// A Generic synchronous Method for deleting many entities
        /// </summary>
        /// <param name="where"></param>
        /// <param name="successMessage"></param>
        /// <returns></returns>
        Result DeleteMany(Expression<Func<TEntity, bool>> where, string successMessage);

        /// <summary>
        /// A Generic asynchronous Method for deleting many entities
        /// </summary>
        /// <param name="where"></param>
        /// <returns></returns>
        Task<Result> DeleteManyAsync(Expression<Func<TEntity, bool>> where, string successMessage);
        #endregion Delete

        #region Count        
        /// <summary>
        /// A Generic synchronous method for counting entities
        /// </summary>
        /// <param name="where"></param>
        /// <returns></returns>
        long Count(Expression<Func<TEntity, bool>> where);

        /// <summary>
        /// A Generic asynchronous method for counting entities
        /// </summary>
        /// <param name="where"></param>
        /// <returns></returns>
        Task<long> CountAsync(Expression<Func<TEntity, bool>> where);
        #endregion Count

        #region Exists        
        /// <summary>
        /// A Generic synchronous method for checking if some entities exist or no (1 = exist, 0 = nonexist, -1 = error occured)
        /// </summary>
        /// <param name="where"></param>
        /// <returns></returns>
        int Exists(Expression<Func<TEntity, bool>> where);

        /// <summary>
        /// A Generic asynchronous method for checking if some entities exist or no (1 = exist, 0 = nonexist, -1 = error occured)
        /// </summary>
        /// <param name="where"></param>
        /// <returns></returns>
        Task<int> ExistsAsync(Expression<Func<TEntity, bool>> where);
        #endregion
    }
}
