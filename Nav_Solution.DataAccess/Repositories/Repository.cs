﻿using Nav_Solution.DataAccess.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using System.Data.Entity.Infrastructure;

namespace Nav_Solution.DataAccess.Repositories
{
    public class Repository<TEntity> : IRepository<TEntity> where TEntity : class, new()
    {
        private readonly DbContext contextFactory;

        public Repository(DbContext _contextFactory)
        {
            if (_contextFactory == null) throw new NullReferenceException();

            contextFactory = _contextFactory;
            contextFactory.Configuration.LazyLoadingEnabled = false;
        }

        #region Get
        public GetResult<TEntity> Get(Expression<Func<TEntity, bool>> where, string successMessage,
            params Expression<Func<TEntity, object>>[] navigationProperties)
        {
            GetResult<TEntity> result = new GetResult<TEntity>();
            IList<TEntity> entities = null;

            try
            {
                IQueryable<TEntity> dbQuery = contextFactory.Set<TEntity>();
                
                if (navigationProperties != null)
                    foreach (Expression<Func<TEntity, object>> navigationProperty in navigationProperties)
                        dbQuery = dbQuery.Include<TEntity, object>(navigationProperty);

                entities = (where != null) ? dbQuery.AsNoTracking().Where(where).ToList() :
                                             dbQuery.AsNoTracking().ToList();

                if (entities != null)
                    result.Entities = entities;

                result.Success = true;
                result.Message = successMessage;
                return result;
            }
            catch (Exception ex)
            {
                result.Message = ex.Message;
                result.ErrorCode = ex.HResult;
                return result;
            }
        }

        public async Task<GetResult<TEntity>> GetAsync(Expression<Func<TEntity, bool>> where, string successMessage,
            params Expression<Func<TEntity, object>>[] navigationProperties)
        {
            GetResult<TEntity> result = new GetResult<TEntity>();
            IList<TEntity> entities = null;

            try
            {
                IQueryable<TEntity> dbQuery = contextFactory.Set<TEntity>();

                if (navigationProperties != null)
                    foreach (Expression<Func<TEntity, object>> navigationProperty in navigationProperties)
                        dbQuery = dbQuery.Include<TEntity, object>(navigationProperty);

                entities = (where != null) ? await dbQuery.AsNoTracking().Where(where).ToListAsync() :
                                             await dbQuery.AsNoTracking().ToListAsync();

                if (entities != null)
                    result.Entities = entities;

                result.Success = true;
                result.Message = successMessage;
                return result;
            }
            catch (Exception ex)
            {
                result.Message = ex.Message;
                result.ErrorCode = ex.HResult;
                return result;
            }
        }

        public GetResult<TEntity> ExecuteProcedure(string successMessage, string procedureName,
            params object[] parameters)
        {
            GetResult<TEntity> result = new GetResult<TEntity>();
            IList<TEntity> entities = null;

            try
            {
                DbSqlQuery<TEntity> dbQuery = contextFactory.Set<TEntity>().SqlQuery(procedureName, parameters);

                entities = dbQuery.AsNoTracking().ToList();

                if (entities != null)
                    result.Entities = entities;

                result.Success = true;
                result.Message = successMessage;
                return result;
            }
            catch (Exception ex)
            {
                result.Message = ex.Message;
                result.ErrorCode = ex.HResult;
                return result;
            }
        }

        public async Task<GetResult<TEntity>> ExecuteProcedureAsync(string successMessage, string procedureName, 
            params object[] parameters)
        {
            GetResult<TEntity> result = new GetResult<TEntity>();
            IList<TEntity> entities = null;

            try
            {                
                DbSqlQuery<TEntity> dbQuery = contextFactory.Set<TEntity>().SqlQuery(procedureName, parameters);                                

                entities = await dbQuery.AsNoTracking().ToListAsync();

                if (entities != null)
                    result.Entities = entities;

                result.Success = true;
                result.Message = successMessage;
                return result;
            }
            catch (Exception ex)
            {
                result.Message = ex.Message;
                result.ErrorCode = ex.HResult;
                return result;
            }            
        }
        #endregion Get

        #region Create        
        public Result AddOne(TEntity item, string successMessage)
        {
            var result = new Result();

            try
            {
                contextFactory.Set<TEntity>().Add(item);
                contextFactory.SaveChanges();

                result.Success = true;
                result.Message = successMessage;
                return result;
            }
            catch (Exception ex)
            {
                result.Message = ex.Message;
                result.ErrorCode = ex.HResult;
                return result;
            }
        }

        public async Task<Result> AddOneAsync(TEntity item, string successMessage)
        {
            var result = new Result();

            try
            {
                contextFactory.Set<TEntity>().Add(item);
                await contextFactory.SaveChangesAsync();

                result.Success = true;
                result.Message = successMessage;
                return result;
            }
            catch (Exception ex)
            {
                result.Message = ex.Message;
                result.ErrorCode = ex.HResult;
                return result;
            }
        }
        #endregion Create

        #region Update                
        public Result UpdateMany(IEnumerable<TEntity> entities, string successMessage)
        {
            var result = new Result();

            try
            {
                if (entities.Count() > 0)
                {
                    foreach (var entity in entities)
                        contextFactory.Entry(entity).State = EntityState.Modified;

                    contextFactory.SaveChanges();

                    result.Success = true;
                    result.Message = successMessage;
                }
                else
                {
                    result.Success = false;
                    result.Message = "There no entities to modify!";
                }

                return result;
            }
            catch (Exception ex)
            {
                result.Message = ex.Message;
                result.ErrorCode = ex.HResult;
                return result;
            }
        }

        public async Task<Result> UpdateManyAsync(IEnumerable<TEntity> entities, string successMessage)
        {
            var result = new Result();

            try
            {
                if (entities.Count() > 0)
                {
                    foreach (var entity in entities)
                        contextFactory.Entry(entity).State = EntityState.Modified;

                    await contextFactory.SaveChangesAsync();

                    result.Success = true;
                    result.Message = successMessage;
                }
                else
                {
                    result.Success = false;
                    result.Message = "There no entities to modify!";
                }

                return result;
            }
            catch (Exception ex)
            {
                result.Message = ex.Message;
                result.ErrorCode = ex.HResult;
                return result;
            }
        }
        #endregion Update

        #region Delete                               
        public Result DeleteMany(Expression<Func<TEntity, bool>> where, string successMessage)
        {
            var result = new Result();

            try
            {
                IQueryable<TEntity> dbQuery = contextFactory.Set<TEntity>();

                var entities = dbQuery.AsNoTracking().Where(where).ToList();

                foreach (var entity in entities)
                {
                    var entry = contextFactory.Entry(entity);
                    if (entry.State == EntityState.Detached)
                        contextFactory.Set<TEntity>().Attach(entity);
                }

                contextFactory.Set<TEntity>().RemoveRange(entities);
                contextFactory.SaveChanges();

                result.Success = true;
                result.Message = successMessage;
                return result;
            }
            catch (Exception ex)
            {
                result.Message = ex.Message;
                result.ErrorCode = ex.HResult;
                return result;
            }
        }

        public async Task<Result> DeleteManyAsync(Expression<Func<TEntity, bool>> where, string successMessage)
        {
            var result = new Result();

            try
            {
                IQueryable<TEntity> dbQuery = contextFactory.Set<TEntity>();

                var entities = await dbQuery.AsNoTracking().Where(where).ToListAsync();

                foreach (var entity in entities)
                {
                    var entry = contextFactory.Entry(entity);
                    if (entry.State == EntityState.Detached)
                        contextFactory.Set<TEntity>().Attach(entity);
                }

                contextFactory.Set<TEntity>().RemoveRange(entities);
                await contextFactory.SaveChangesAsync();

                result.Success = true;
                result.Message = successMessage;
                return result;
            }
            catch (Exception ex)
            {
                result.Message = ex.Message;
                result.ErrorCode = ex.HResult;
                return result;
            }
        }
        #endregion Delete

        #region Count                
        public long Count(Expression<Func<TEntity, bool>> where)
        {
            try
            {
                var results = Get(where, "OK");
                return results.Entities.Count();
            }
            catch (Exception)
            {
                return -1;
            }
        }

        public async Task<long> CountAsync(Expression<Func<TEntity, bool>> where)
        {
            try
            {
                var results = await GetAsync(where, "OK");
                return results.Entities.Count();
            }
            catch (Exception)
            {
                return -1;
            }
        }
        #endregion Count

        #region Exists                
        public int Exists(Expression<Func<TEntity, bool>> where)
        {
            try
            {
                var count = Count(where);
                return (count > 0) ? 1 : 0;
            }
            catch (Exception)
            {
                return -1;
            }
        }

        public async Task<int> ExistsAsync(Expression<Func<TEntity, bool>> where)
        {
            try
            {
                var count = await CountAsync(where);
                return (count > 0) ? 1 : 0;
            }
            catch (Exception)
            {
                return -1;
            }
        }
        #endregion
    }
}
