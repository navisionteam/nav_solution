﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Nav_Solution.DomainModel.Models
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Infrastructure;
    using System.Data.Entity.Core.Objects;
    using System.Linq;
    
    public partial class NavSolutionEntities : DbContext
    {
        public NavSolutionEntities()
            : base("name=NavSolutionEntities")
        {
        }
    
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            throw new UnintentionalCodeFirstException();
        }
    
        public virtual DbSet<tbl_Account_Formulas> tbl_Account_Formulas { get; set; }
        public virtual DbSet<tbl_Main_account> tbl_Main_account { get; set; }
        public virtual DbSet<tbl_Users> tbl_Users { get; set; }
        public virtual DbSet<template> templates { get; set; }
        public virtual DbSet<template_Layout> template_Layout { get; set; }
        public virtual DbSet<tbl_related_Acc> tbl_related_Acc { get; set; }
        public virtual DbSet<tbl_related_Loc> tbl_related_Loc { get; set; }
        public virtual DbSet<CostCenter> CostCenters { get; set; }
        public virtual DbSet<v_templete_MainAccount> v_templete_MainAccount { get; set; }
    
        public virtual ObjectResult<pro_Get_Company_Result> pro_Get_Company()
        {
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction<pro_Get_Company_Result>("pro_Get_Company");
        }
    
        public virtual ObjectResult<Pro_GetCostCenter_Result> Pro_GetCostCenter()
        {
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction<Pro_GetCostCenter_Result>("Pro_GetCostCenter");
        }
    
        public virtual int SP_Ass_Data(string dbName, Nullable<System.DateTime> sT_Date, Nullable<System.DateTime> end_Date, string selectedCompanies, string costCenter, Nullable<decimal> userCode, Nullable<int> reportType)
        {
            var dbNameParameter = dbName != null ?
                new ObjectParameter("dbName", dbName) :
                new ObjectParameter("dbName", typeof(string));
    
            var sT_DateParameter = sT_Date.HasValue ?
                new ObjectParameter("ST_Date", sT_Date) :
                new ObjectParameter("ST_Date", typeof(System.DateTime));
    
            var end_DateParameter = end_Date.HasValue ?
                new ObjectParameter("End_Date", end_Date) :
                new ObjectParameter("End_Date", typeof(System.DateTime));
    
            var selectedCompaniesParameter = selectedCompanies != null ?
                new ObjectParameter("SelectedCompanies", selectedCompanies) :
                new ObjectParameter("SelectedCompanies", typeof(string));
    
            var costCenterParameter = costCenter != null ?
                new ObjectParameter("CostCenter", costCenter) :
                new ObjectParameter("CostCenter", typeof(string));
    
            var userCodeParameter = userCode.HasValue ?
                new ObjectParameter("UserCode", userCode) :
                new ObjectParameter("UserCode", typeof(decimal));
    
            var reportTypeParameter = reportType.HasValue ?
                new ObjectParameter("ReportType", reportType) :
                new ObjectParameter("ReportType", typeof(int));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction("SP_Ass_Data", dbNameParameter, sT_DateParameter, end_DateParameter, selectedCompaniesParameter, costCenterParameter, userCodeParameter, reportTypeParameter);
        }
    
        public virtual int Pro_CC(string cC)
        {
            var cCParameter = cC != null ?
                new ObjectParameter("CC", cC) :
                new ObjectParameter("CC", typeof(string));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction("Pro_CC", cCParameter);
        }
    
        public virtual int Pro_Get_Formula_Systax()
        {
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction("Pro_Get_Formula_Systax");
        }
    
        public virtual int Pro_Pivot()
        {
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction("Pro_Pivot");
        }
    
        public virtual int Pro_Update_formula_details(string sourceName)
        {
            var sourceNameParameter = sourceName != null ?
                new ObjectParameter("SourceName", sourceName) :
                new ObjectParameter("SourceName", typeof(string));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction("Pro_Update_formula_details", sourceNameParameter);
        }
    
        public virtual int Pro_Update_formula_details_Prev(string sourceName)
        {
            var sourceNameParameter = sourceName != null ?
                new ObjectParameter("SourceName", sourceName) :
                new ObjectParameter("SourceName", typeof(string));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction("Pro_Update_formula_details_Prev", sourceNameParameter);
        }
    
        public virtual int SP_Ass_CC(string cC)
        {
            var cCParameter = cC != null ?
                new ObjectParameter("CC", cC) :
                new ObjectParameter("CC", typeof(string));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction("SP_Ass_CC", cCParameter);
        }
    
        public virtual int SP_Ass_Data0(Nullable<int> reportType, ObjectParameter @int)
        {
            var reportTypeParameter = reportType.HasValue ?
                new ObjectParameter("ReportType", reportType) :
                new ObjectParameter("ReportType", typeof(int));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction("SP_Ass_Data0", reportTypeParameter, @int);
        }
    
        public virtual int SP_Ass_Data2(string dbName, Nullable<System.DateTime> sT_Date, Nullable<System.DateTime> end_Date, string selectedCompanies, string costCenter, Nullable<decimal> userCode, Nullable<int> reportType)
        {
            var dbNameParameter = dbName != null ?
                new ObjectParameter("dbName", dbName) :
                new ObjectParameter("dbName", typeof(string));
    
            var sT_DateParameter = sT_Date.HasValue ?
                new ObjectParameter("ST_Date", sT_Date) :
                new ObjectParameter("ST_Date", typeof(System.DateTime));
    
            var end_DateParameter = end_Date.HasValue ?
                new ObjectParameter("End_Date", end_Date) :
                new ObjectParameter("End_Date", typeof(System.DateTime));
    
            var selectedCompaniesParameter = selectedCompanies != null ?
                new ObjectParameter("SelectedCompanies", selectedCompanies) :
                new ObjectParameter("SelectedCompanies", typeof(string));
    
            var costCenterParameter = costCenter != null ?
                new ObjectParameter("CostCenter", costCenter) :
                new ObjectParameter("CostCenter", typeof(string));
    
            var userCodeParameter = userCode.HasValue ?
                new ObjectParameter("UserCode", userCode) :
                new ObjectParameter("UserCode", typeof(decimal));
    
            var reportTypeParameter = reportType.HasValue ?
                new ObjectParameter("ReportType", reportType) :
                new ObjectParameter("ReportType", typeof(int));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction("SP_Ass_Data2", dbNameParameter, sT_DateParameter, end_DateParameter, selectedCompaniesParameter, costCenterParameter, userCodeParameter, reportTypeParameter);
        }
    
        public virtual int SP_CC(string selectedCC, ObjectParameter @int)
        {
            var selectedCCParameter = selectedCC != null ?
                new ObjectParameter("SelectedCC", selectedCC) :
                new ObjectParameter("SelectedCC", typeof(string));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction("SP_CC", selectedCCParameter, @int);
        }
    
        public virtual int SP_CC2(string selectedCC, ObjectParameter @int)
        {
            var selectedCCParameter = selectedCC != null ?
                new ObjectParameter("SelectedCC", selectedCC) :
                new ObjectParameter("SelectedCC", typeof(string));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction("SP_CC2", selectedCCParameter, @int);
        }
    
        public virtual int SP_COCE(ObjectParameter @int)
        {
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction("SP_COCE", @int);
        }
    
        public virtual int SP_Com(ObjectParameter @int)
        {
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction("SP_Com", @int);
        }
    
        public virtual int SP_Form_synt(ObjectParameter @int)
        {
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction("SP_Form_synt", @int);
        }
    
        public virtual int SP_G_L_SCr(ObjectParameter @int)
        {
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction("SP_G_L_SCr", @int);
        }
    
        public virtual int SP_PL2(string dbName, Nullable<System.DateTime> sT_Date, Nullable<System.DateTime> end_Date, string selectedCompanies, string costCenter, Nullable<decimal> userCode, Nullable<int> reportType)
        {
            var dbNameParameter = dbName != null ?
                new ObjectParameter("dbName", dbName) :
                new ObjectParameter("dbName", typeof(string));
    
            var sT_DateParameter = sT_Date.HasValue ?
                new ObjectParameter("ST_Date", sT_Date) :
                new ObjectParameter("ST_Date", typeof(System.DateTime));
    
            var end_DateParameter = end_Date.HasValue ?
                new ObjectParameter("End_Date", end_Date) :
                new ObjectParameter("End_Date", typeof(System.DateTime));
    
            var selectedCompaniesParameter = selectedCompanies != null ?
                new ObjectParameter("SelectedCompanies", selectedCompanies) :
                new ObjectParameter("SelectedCompanies", typeof(string));
    
            var costCenterParameter = costCenter != null ?
                new ObjectParameter("CostCenter", costCenter) :
                new ObjectParameter("CostCenter", typeof(string));
    
            var userCodeParameter = userCode.HasValue ?
                new ObjectParameter("UserCode", userCode) :
                new ObjectParameter("UserCode", typeof(decimal));
    
            var reportTypeParameter = reportType.HasValue ?
                new ObjectParameter("ReportType", reportType) :
                new ObjectParameter("ReportType", typeof(int));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction("SP_PL2", dbNameParameter, sT_DateParameter, end_DateParameter, selectedCompaniesParameter, costCenterParameter, userCodeParameter, reportTypeParameter);
        }
    
        public virtual int SP_test(string dbName, Nullable<System.DateTime> sT_Date, Nullable<System.DateTime> end_Date, string selectedCompanies, string costCenter, Nullable<decimal> userCode, Nullable<int> reportType)
        {
            var dbNameParameter = dbName != null ?
                new ObjectParameter("dbName", dbName) :
                new ObjectParameter("dbName", typeof(string));
    
            var sT_DateParameter = sT_Date.HasValue ?
                new ObjectParameter("ST_Date", sT_Date) :
                new ObjectParameter("ST_Date", typeof(System.DateTime));
    
            var end_DateParameter = end_Date.HasValue ?
                new ObjectParameter("End_Date", end_Date) :
                new ObjectParameter("End_Date", typeof(System.DateTime));
    
            var selectedCompaniesParameter = selectedCompanies != null ?
                new ObjectParameter("SelectedCompanies", selectedCompanies) :
                new ObjectParameter("SelectedCompanies", typeof(string));
    
            var costCenterParameter = costCenter != null ?
                new ObjectParameter("CostCenter", costCenter) :
                new ObjectParameter("CostCenter", typeof(string));
    
            var userCodeParameter = userCode.HasValue ?
                new ObjectParameter("UserCode", userCode) :
                new ObjectParameter("UserCode", typeof(decimal));
    
            var reportTypeParameter = reportType.HasValue ?
                new ObjectParameter("ReportType", reportType) :
                new ObjectParameter("ReportType", typeof(int));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction("SP_test", dbNameParameter, sT_DateParameter, end_DateParameter, selectedCompaniesParameter, costCenterParameter, userCodeParameter, reportTypeParameter);
        }
    }
}
