﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Nav_Solution.DomainModel.Models;

namespace Nav_Solution.UI
{
    /// <summary>
    /// Interaction logic for AccountSetup.xaml
    /// </summary>
    /// 
    public partial class AccountSetup : Page
    {
        string Pid;
        int rid,Lid;
    
        NavSolutionEntities db = new NavSolutionEntities();
        public AccountSetup(string id)
        {
            try
            {
                Pid = id;
                InitializeComponent();
                Datagrid1.ItemsSource = db.tbl_related_Acc.Where(p => p.MainAccountId.ToString() == Pid).ToList();
                Datagrid2.ItemsSource = db.tbl_related_Loc.Where(p => p.RelatedAccountID.ToString() == Pid).ToList();
                ContentView2.Content = new NewFormula(Pid.ToString());
            }
            catch { }
        }

        private void DataGridCell_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            try
            {
                var dataGridCellTarget = (DataGridRow)sender;
                var data = ((tbl_related_Acc)dataGridCellTarget.DataContext);
                rid = data.id;
                Mainaccountid.Text = data.MainAccountId.ToString();
                From.Text = data.AccountNumFrom.ToString();
                To.Text = data.AccountNumTo.ToString();
                Note.Text = data.Note?.ToString();
                Sign.Text = data.Sign.ToString();
                //HasFormula.Text = data.tbl_Main_account.tbl_Account_Formulas.Count > 0 ? "Yes" : "No";
            }
            catch { }
        }

        private void Save_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (rid != 0)
                {
                    var acc = db.tbl_related_Acc.Find(rid);
                    acc.MainAccountId = int.Parse(Mainaccountid.Text);
                    acc.AccountNumFrom = From.Text;
                    acc.AccountNumTo = To.Text;
                    acc.Note = Note.Text;
                    acc.Sign = int.Parse(Sign.Text);
                    db.SaveChanges();
                }
                else
                {
                    var acc = new tbl_related_Acc();
                    acc.MainAccountId = int.Parse(Mainaccountid.Text);
                    acc.AccountNumFrom = From.Text;
                    acc.AccountNumTo = To.Text;
                    acc.Note = Note.Text;
                    acc.Sign = int.Parse(Sign.Text);
                    db.tbl_related_Acc.Add(acc);
                    db.SaveChanges();
                }
                Datagrid1.ItemsSource = db.tbl_related_Acc.Where(p => p.MainAccountId.ToString() == Pid).ToList();
                Mainaccountid.Text = "";
                From.Text = "";
                To.Text = "";
                Note.Text = "";
                rid = 0;
                MessageBox.Show("Account Data Saved");
                //HasFormula.Text = "";
            }
            catch { }
        }

        private void Delete_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (rid != 0)
                {
                    var acc = db.tbl_related_Acc.Find(rid);
                    db.tbl_related_Acc.Remove(acc);
                    db.SaveChanges();
                    rid = 0;
                }
            }
            catch
            {

            }
        }

        private void SearchBox2_TextChanged(object sender, TextChangedEventArgs e)
        {

        }

        private void Save2_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (Lid != 0)
                {
                    var acc = db.tbl_related_Loc.Find(Lid);
                    acc.RelatedAccountID = int.Parse(LMainaccountid.Text);
                    acc.LocationnameFrom = LFrom.Text;
                    acc.LocationnameTo = LTo.Text;
                    db.SaveChanges();
                }
                else
                {
                    var acc = new tbl_related_Loc();
                    acc.RelatedAccountID = int.Parse(LMainaccountid.Text);
                    acc.LocationnameFrom = LFrom.Text;
                    acc.LocationnameTo = LTo.Text;
                    db.tbl_related_Loc.Add(acc);
                    db.SaveChanges();
                }
                Datagrid2.ItemsSource = db.tbl_related_Loc.Where(p => p.RelatedAccountID.ToString() == Pid).ToList();
                LMainaccountid.Text = "";
                LFrom.Text = "";
                LTo.Text = "";
                Lid = 0;
                MessageBox.Show("Account Data Saved");
                //HasFormula.Text = "";
            }
            catch { }
        }


        private void DataGridRow_MouseDoubleClick_1(object sender, MouseButtonEventArgs e)
        {
                var dataGridCellTarget = (DataGridRow)sender;
            try
            {
                var data = ((tbl_related_Loc)dataGridCellTarget.DataContext);
                Lid = data.id;
                LMainaccountid.Text = data.RelatedAccountID.ToString();
                LFrom.Text = data.LocationnameFrom.ToString();
                LTo.Text = data.LocationnameTo.ToString();
            }
            catch { }

        }

        private void Delete2_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (Lid != 0)
                {
                    var acc = db.tbl_related_Loc.Find(rid);
                    db.tbl_related_Loc.Remove(acc);
                    db.SaveChanges();
                    Lid = 0;
                }
            }
            catch { }
        }
    }
}
