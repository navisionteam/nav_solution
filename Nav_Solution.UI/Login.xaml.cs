﻿using Nav_Solution.DataAccess.Repositories;
using Nav_Solution.DomainModel.Models;
using System;
using System.Windows;

namespace Nav_Solution.UI
{
    /// <summary>
    /// Interaction logic for Login.xaml
    /// </summary>
    public partial class Login : Window
    {
        IRepository<tbl_Users> userRepo;

        public Login()
        {
            InitializeComponent();
            userRepo = new Repository<tbl_Users>(new NavSolutionEntities());
        }

        private void Window_Closed(object sender, EventArgs e)
        {
            Application.Current.Shutdown();
        }

        private async void btnLogin_Click(object sender, RoutedEventArgs e)
        {
            string username = txtUsername.Text;
            string password = txtPassword.Password;
            var user = await userRepo.GetAsync(u => u.UserName == username && 
                                                    u.Password == password, 
                                                    "Success", null);
            if (user.Entities != null && ((System.Collections.Generic.List<Nav_Solution.DomainModel.Models.tbl_Users>)user.Entities).Count >0)
            {
                Main main = new Main(username);
                this.Hide();
                main.Show();                
            }
            else            
                MessageBox.Show("Invalid Username or Password");            
        }
    }
}