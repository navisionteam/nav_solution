﻿using Nav_Solution.UI.Reports;
using System;
using System.Windows;
using System.Windows.Controls;

namespace Nav_Solution.UI
{
    /// <summary>
    /// Interaction logic for Main.xaml
    /// </summary>
    public partial class Main : Window
    {
        #region Declaration
        ReportContainer reportContainer;
        Window1 setupxaml;

        private bool _SetupShow;
        #endregion

        public Main(string username )
        {
          
            InitializeComponent();
            if (username.ToLower() == "admin")
            {
                setupmenu.Visibility = Visibility.Visible;
            }
            else
            {
                setupmenu.Visibility = Visibility.Collapsed;

            }
        }
        public bool SetupShow
        {
            get { return _SetupShow; }
        }
        private void Window_Closed(object sender, EventArgs e)
        {

            Application.Current.Shutdown();
        }

        private void ShowReportContainer_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                reportContainer = new ReportContainer();
                //ReportTemplate report = new ReportTemplate();
                var Sender = (MenuItem)sender;
                reportContainer.SenderName = Sender.Name;
                reportContainer.Visibility = Visibility.Visible;
                ContentView.Content = reportContainer;
                
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.InnerException.Message);
            }
           
        }

        private void Setup_Click(object sender, RoutedEventArgs e)
        {
            setupxaml = new Window1();
            setupxaml.WindowStartupLocation = System.Windows.WindowStartupLocation.CenterScreen;
            setupxaml.Show();

        }
    }
}
