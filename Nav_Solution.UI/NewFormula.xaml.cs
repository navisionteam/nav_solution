﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Nav_Solution.DomainModel.Models;

namespace Nav_Solution.UI
{
    /// <summary>
    /// Interaction logic for NewFormula.xaml
    /// </summary>
    public class TestClass2
    {
        public string id { get; set; }
        public string name { get; set; }
        public string FromAccount { get; set; }
        public string ToAcount { get; set; }
    }
    public partial class NewFormula : Page
    {
        string AID;
        string MID;
        NavSolutionEntities db = new NavSolutionEntities();
        
        public NewFormula(string id)
        {
            InitializeComponent();
          
            var formu = db.tbl_Account_Formulas.Where(p => p.Main_Account_id.ToString() == id).FirstOrDefault();
            Formula.Text = formu?.Formula;
            Percentage.Text = formu?.Percentage;
            Condition.Text = formu?.Condition;
            if (formu?.Credit != null)
            Credit.IsChecked = formu?.Credit.Value == 1;
            if (formu?.Debit != null)
                Depit.IsChecked = formu?.Debit.Value == 1;
            AID = formu?.id.ToString();
            MID = id;
            loaddata("");
        }
        object selectedtext ;
        private void SearchBox_TextChanged(object sender, TextChangedEventArgs e)
        {
            loaddata(((TextBox)sender).Text);
        }
        private void DataGridCell_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            string formulaa="";
             int last =0;
            var formu = db.tbl_Account_Formulas.Where(p => p.Main_Account_id.ToString() == ((TestClass2)((DataGridRow)sender).DataContext).id).FirstOrDefault();
            if (string.IsNullOrEmpty(formu?.Formula))
            {
                var relid = db.tbl_related_Acc.Where(m => m.MainAccountId.ToString() == ((TestClass2)((DataGridRow)sender).DataContext).id).ToList();
                for (int i = 0; i < relid.Count; i++)
                {
                    formulaa += "[" + relid[i].id + "]";
                    if (i + 1 != relid.Count)
                        formulaa += "+";
                }
             
            }
            else
            {
                if (formu.Formula.Contains("-") || formu.Formula.Contains("+") || formu.Formula.Contains("*") || formu.Formula.Contains("+"))
                {
                    if (formu.tbl_Main_account.tbl_related_Acc.First().AccountNumFrom == "0")
                    {
                        var relid = db.tbl_related_Acc.Where(m => m.MainAccountId.ToString() == ((TestClass2)((DataGridRow)sender).DataContext).id).ToList();
                        formulaa += "[" + relid[0].id + "]";
                    }
                    else
                        formulaa += "("+formu?.Formula +")";
                }
                else

                    formulaa += formu?.Formula;


            }
            try
            {
                ((TextBox)selectedtext).Text += formulaa;
                ((TextBox)selectedtext).Focus();
                ((TextBox)selectedtext).CaretIndex = Formula.Text.Length;
            }
            catch
            {
                Formula.Text += formulaa;
                Formula.Focus();
                Formula.CaretIndex = Formula.Text.Length;
            }
        }
        void loaddata(string search)
        {
            TestData = new ObservableCollection<TestClass2>();
            if (search == "")
            {
                var datas = db.tbl_Main_account.ToList();
                foreach (var data in datas)
                {
                    TestData.Add(new TestClass2 { id = data.id.ToString(), name = data.name });
                }
                Datagrid1.ItemsSource = TestData;
            }
            else
            {
                var datas = db.tbl_Main_account.Where(p => p.name.Contains(search) || p.id.ToString() == search || p.name_Eng.Contains(search)).ToList();
                foreach (var data in datas)
                {
                    TestData.Add(new TestClass2 { id = data.id.ToString(), name = data.name});
                }
                Datagrid1.ItemsSource = TestData;
            }
        }
        private ObservableCollection<TestClass2> _testData = new ObservableCollection<TestClass2>();
        public ObservableCollection<TestClass2> TestData
        {
            get { return _testData; }
            set { _testData = value; }
        }

        private void Save_Click(object sender, RoutedEventArgs e)
        {
            if (AID != null)
            db.tbl_Account_Formulas.Find(int.Parse(AID)).Formula = Formula.Text;
            else
            {
                tbl_Account_Formulas forma = new tbl_Account_Formulas();
                forma.Main_Account_id = int.Parse(MID);
                forma.Formula = Formula.Text;
                forma.Condition = Condition.Text;
                forma.Percentage = Percentage.Text;
                if (Depit.IsChecked.Value)
                {
                    forma.Credit = 0;
                    forma.Debit = 1;
                }
                else if (Depit.IsChecked.Value)
                {
                    forma.Credit = 1;
                    forma.Debit = 0;
                }
                else
                {
                    forma.Credit = 0;
                    forma.Debit = 0;
                }
                db.tbl_Account_Formulas.Add(forma);
            }
            db.SaveChanges();
            MessageBox.Show("Account Data Saved");
        }

        private void delete_Click(object sender, RoutedEventArgs e)
        {

        }

        private void button_Click(object sender, RoutedEventArgs e)
        {
           Credit.IsChecked = false;
            Depit.IsChecked = false;
        }

        private void Formula_GotFocus(object sender, RoutedEventArgs e)
        {
            selectedtext = sender;
        }

        private void Percentage_GotFocus(object sender, RoutedEventArgs e)
        {
            selectedtext = sender;
        }

        private void Condition_GotFocus(object sender, RoutedEventArgs e)
        {
            selectedtext = sender;
        }
    }
}
