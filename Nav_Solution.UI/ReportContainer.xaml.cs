﻿using CrystalDecisions.Shared;
using Nav_Solution.UI.Reports;
using Nav_Solution.UI.Temp;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text.RegularExpressions;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;

namespace Nav_Solution.UI
{
    /// <summary>
    /// Interaction logic for ReportContainer.xaml
    /// </summary>
    public partial class ReportContainer : UserControl
    {
        #region Prooerties
        public string SenderName { get; set; }
        public List<string> CompanyNames { get; set; }
        public List<string> CostCenterNames { get; set; }
        #endregion

        public ReportContainer()
        {
            try
            {
                InitializeComponent();
                this.DataContext = this;
                LoadCompanies();
                LoadCostCenters();
                CompanyNames = new List<string>();
                CostCenterNames = new List<string>();
            }
            catch (System.Exception ex)
            {
                MessageBox.Show(ex.InnerException.Message);
            }
        }

        private bool IsTextAllowed(string text)
        {
            Regex regex = new Regex("[^0-9/]+");
            return !regex.IsMatch(text);
        }

        private void DatePicker_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            e.Handled = !IsTextAllowed(e.Text);
        }

        private void UserControl_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            reportViewer.Height = this.ActualHeight - 180;
            CompanyListBox.Height = this.ActualHeight - 200;
            CostCenterListBox.Height = this.ActualHeight - 200;
        }
        public static TableLogOnInfo GetODBCTableLogOnInfo(string ODBCName, string serverName, string databaseName, string userID, string password)
        {
            CrystalDecisions.ReportAppServer.DataDefModel.PropertyBag connectionAttributes = new CrystalDecisions.ReportAppServer.DataDefModel.PropertyBag();

            connectionAttributes.EnsureCapacity(3);
            connectionAttributes.Add(DbConnectionAttributes.CONNINFO_CONNECTION_STRING, string.Format("DSN={0};Driver={{SQL Server}}", ODBCName));
            connectionAttributes.Add("Server", serverName);
            connectionAttributes.Add("UseDSNProperties", false);

            DbConnectionAttributes attributes = new DbConnectionAttributes();
            attributes.Collection.Add(new NameValuePair2("Database DLL", "crdb_odbc.dll"));
            attributes.Collection.Add(new NameValuePair2("QE_DatabaseName", databaseName));
            attributes.Collection.Add(new NameValuePair2("QE_DatabaseType", "ODBC (RDO)"));
            attributes.Collection.Add(new NameValuePair2("QE_LogonProperties", connectionAttributes));
            attributes.Collection.Add(new NameValuePair2("QE_ServerDescription", serverName));
            attributes.Collection.Add(new NameValuePair2("QE_SQLDB", true));
            attributes.Collection.Add(new NameValuePair2("SSO Enabled", false));

            ConnectionInfo connectionInfo = GetConnectionInfo(serverName, databaseName, userID, password);
            connectionInfo.Attributes = attributes;
            connectionInfo.Type = ConnectionInfoType.CRQE;
            connectionInfo.IntegratedSecurity = false;
            TableLogOnInfo tableLogOnInfo = new TableLogOnInfo();
            tableLogOnInfo.ConnectionInfo = connectionInfo;
            return tableLogOnInfo;
        }

        public static ConnectionInfo GetConnectionInfo(string serverName, string databaseName, string userID, string password)
        {
            ConnectionInfo connectionInfo = new ConnectionInfo();
            connectionInfo.ServerName = serverName;
            connectionInfo.DatabaseName = databaseName;
            connectionInfo.UserID = userID;
            connectionInfo.Password = password;

            return connectionInfo;
        }

        private void btnSearch_Click(object sender, RoutedEventArgs e)
        {
            if (!dateFrom.SelectedDate.HasValue || !dateTo.SelectedDate.HasValue)
                MessageBox.Show("Select Start Date & End Date");
            else if (dateFrom.SelectedDate.Value > dateTo.SelectedDate.Value)
                MessageBox.Show("Start Date must be less than (or equal to) End Date");
            else
            {
                string SelectedCompanies = string.Empty;
                string SelectedCostCenters = string.Empty;

                switch (SenderName)
                {
                    case "PlTamaraConcept":
                        PL_TAMARA_CONCEPT pLTamaraConcept = new PL_TAMARA_CONCEPT();
                        pLTamaraConcept.Load("Reports/PL TAMARA CONCEPT.rpt");
                        var CrTables = pLTamaraConcept.Database.Tables;
                        TableLogOnInfo crtableLogoninfo = new TableLogOnInfo();
                          
                        pLTamaraConcept.SetParameterValue("@ReportType", 5);
                        pLTamaraConcept.SetParameterValue("@dbName", "Mori");
                        pLTamaraConcept.SetParameterValue("@ST_Date", dateFrom.SelectedDate.Value);
                        pLTamaraConcept.SetParameterValue("@End_Date", dateTo.SelectedDate.Value);
                        foreach (var companyName in CompanyNames)
                            SelectedCompanies += "[" + companyName + "]";
                        foreach (var costCenterName in CostCenterNames)
                            SelectedCostCenters += "[" + costCenterName + "]";
                        pLTamaraConcept.SetParameterValue("@SelectedCompanies", SelectedCompanies);
                        pLTamaraConcept.SetParameterValue("@CostCenter", SelectedCostCenters);
                        pLTamaraConcept.SetParameterValue("@UserCode", 7418);
                        
                        reportViewer.ViewerCore.ReportSource = pLTamaraConcept;
                        reportViewer.ViewerCore.LogOnInfo = new TableLogOnInfos();
                        reportViewer.ViewerCore.LogOnInfo.Add(GetODBCTableLogOnInfo("Mori", "DESKTOP-SPSH5MS\\MSSQLSERVER2014", "Nav_Dwh", "sa", "retail_ps2010"));
                        break;
                    case "PlTrend":
                        PL_TREND PL_TRENDs = new PL_TREND();
                        PL_TRENDs.Load("Reports/PL_TREND.rpt");
                        PL_TRENDs.SetParameterValue("@dbName", "Mori");
                        PL_TRENDs.SetParameterValue("@ST_Date", dateFrom.SelectedDate.Value);
                        PL_TRENDs.SetParameterValue("@End_Date", dateTo.SelectedDate.Value);
                        foreach (var companyName in CompanyNames)
                            SelectedCompanies += "[" + companyName + "]";
                        foreach (var costCenterName in CostCenterNames)
                            SelectedCostCenters += "[" + costCenterName + "]";
                        PL_TRENDs.SetParameterValue("@SelectedCompanies", SelectedCompanies);
                        PL_TRENDs.SetParameterValue("@CostCenter", SelectedCostCenters);
                        PL_TRENDs.SetParameterValue("@UserCode", 7418);
                        PL_TRENDs.SetParameterValue("@ReportType", 8);
                        PL_TRENDs.SetDatabaseLogon("sa", "P@ssw0rd");
                        reportViewer.ViewerCore.ReportSource = PL_TRENDs;
                        reportViewer.ViewerCore.LogOnInfo = new TableLogOnInfos();
                        reportViewer.ViewerCore.LogOnInfo.Add(GetODBCTableLogOnInfo("Mori", "DESKTOP-SPSH5MS\\MSSQLSERVER2014", "Nav_Dwh", "sa", "retail_ps2010"));
                        break;
                    case "PlSbsMon":
                        PL2017_SBS_MON pL2017SbsMon = new PL2017_SBS_MON();
                        pL2017SbsMon.Load("Reports/PL2017 SBS MON.rpt");
                        pL2017SbsMon.SetParameterValue("@dbName", "Mori");
                        pL2017SbsMon.SetParameterValue("@ST_Date", dateFrom.SelectedDate.Value);
                        pL2017SbsMon.SetParameterValue("@End_Date", dateTo.SelectedDate.Value);
                        foreach (var companyName in CompanyNames)
                            SelectedCompanies += "[" + companyName + "]";

                        foreach (var costCenterName in CostCenterNames)
                            SelectedCostCenters += "[" + costCenterName + "]";

                        pL2017SbsMon.SetParameterValue("@SelectedCompanies", SelectedCompanies);
                        pL2017SbsMon.SetParameterValue("@CostCenter", SelectedCostCenters);
                        pL2017SbsMon.SetParameterValue("@UserCode", 7418);
                        pL2017SbsMon.SetParameterValue("@ReportType", 4);
                        pL2017SbsMon.SetDatabaseLogon("sa", "P@ssw0rd");
                        reportViewer.ViewerCore.ReportSource = pL2017SbsMon;
                        reportViewer.ViewerCore.LogOnInfo = new TableLogOnInfos();
                        reportViewer.ViewerCore.LogOnInfo.Add(GetODBCTableLogOnInfo("Mori", "DESKTOP-SPSH5MS\\MSSQLSERVER2014", "Nav_Dwh", "sa", "retail_ps2010"));
                        break;
                    case "PlSbsYtd":
                        PL2017_SBS_YTD PL2017_SBS_YTDs = new PL2017_SBS_YTD();
                        PL2017_SBS_YTDs.Load("Reports/PL2017 SBS YTD.rpt");
                        PL2017_SBS_YTDs.SetParameterValue("@dbName", "Mori");
                        PL2017_SBS_YTDs.SetParameterValue("@ST_Date", dateFrom.SelectedDate.Value);
                        PL2017_SBS_YTDs.SetParameterValue("@End_Date", dateTo.SelectedDate.Value);


                        foreach (var companyName in CompanyNames)
                            SelectedCompanies += "[" + companyName + "]";

                        foreach (var costCenterName in CostCenterNames)
                            SelectedCostCenters += "[" + costCenterName + "]";

                        PL2017_SBS_YTDs.SetParameterValue("@SelectedCompanies", SelectedCompanies);
                        PL2017_SBS_YTDs.SetParameterValue("@CostCenter", SelectedCostCenters);
                        PL2017_SBS_YTDs.SetParameterValue("@UserCode", 7418);
                        PL2017_SBS_YTDs.SetParameterValue("@ReportType", 9);
                        PL2017_SBS_YTDs.SetDatabaseLogon("sa", "P@ssw0rd");
                        reportViewer.ViewerCore.ReportSource = PL2017_SBS_YTDs;
                        reportViewer.ViewerCore.LogOnInfo = new TableLogOnInfos();
                        reportViewer.ViewerCore.LogOnInfo.Add(GetODBCTableLogOnInfo("Mori", "DESKTOP-SPSH5MS\\MSSQLSERVER2014", "Nav_Dwh", "sa", "retail_ps2010"));
                        break;
                    case "AdminExpTrend":
                        ADMIN_EXP_TREND adminExpTrend = new ADMIN_EXP_TREND();
                        adminExpTrend.Load("Reports/Admin Exp Trend.rpt");
                        adminExpTrend.SetParameterValue("@dbName", "Mori");
                        adminExpTrend.SetParameterValue("@ST_Date", dateFrom.SelectedDate.Value);
                        adminExpTrend.SetParameterValue("@End_Date", dateTo.SelectedDate.Value);


                        foreach (var companyName in CompanyNames)
                            SelectedCompanies += "[" + companyName + "]";

                        foreach (var costCenterName in CostCenterNames)
                            SelectedCostCenters += "[" + costCenterName + "]";

                        adminExpTrend.SetParameterValue("@SelectedCompanies", SelectedCompanies);
                        adminExpTrend.SetParameterValue("@CostCenter", SelectedCostCenters);
                        adminExpTrend.SetParameterValue("@UserCode", 7418);
                        adminExpTrend.SetParameterValue("@ReportType", 2);
                        adminExpTrend.SetDatabaseLogon("sa", "P@ssw0rd");
                        reportViewer.ViewerCore.ReportSource = adminExpTrend;
                        reportViewer.ViewerCore.LogOnInfo = new TableLogOnInfos();
                        reportViewer.ViewerCore.LogOnInfo.Add(GetODBCTableLogOnInfo("Mori", "DESKTOP-SPSH5MS\\MSSQLSERVER2014", "Nav_Dwh", "sa", "retail_ps2010"));
                        break;
                    case "ExpDepYtdSbsVsBud":
                        EXP__DEP_YTD_SBS_VS_BUD EXP__DEP_YTD_SBS_VS_BUDs = new EXP__DEP_YTD_SBS_VS_BUD();
                        EXP__DEP_YTD_SBS_VS_BUDs.Load("Reports/EXP DEP YTD SBS VS BUD.rpt");
                        EXP__DEP_YTD_SBS_VS_BUDs.SetParameterValue("@dbName", "Mori");
                        EXP__DEP_YTD_SBS_VS_BUDs.SetParameterValue("@ST_Date", dateFrom.SelectedDate.Value);
                        EXP__DEP_YTD_SBS_VS_BUDs.SetParameterValue("@End_Date", dateTo.SelectedDate.Value);
                        foreach (var companyName in CompanyNames)
                            SelectedCompanies += "[" + companyName + "]";
                        foreach (var costCenterName in CostCenterNames)
                            SelectedCostCenters += "[" + costCenterName + "]";
                        EXP__DEP_YTD_SBS_VS_BUDs.SetParameterValue("@SelectedCompanies", SelectedCompanies);
                        EXP__DEP_YTD_SBS_VS_BUDs.SetParameterValue("@CostCenter", SelectedCostCenters);
                        EXP__DEP_YTD_SBS_VS_BUDs.SetParameterValue("@UserCode", 7418);
                        EXP__DEP_YTD_SBS_VS_BUDs.SetParameterValue("@ReportType", 3);
                        EXP__DEP_YTD_SBS_VS_BUDs.SetDatabaseLogon("sa", "P@ssw0rd");
                        reportViewer.ViewerCore.ReportSource = EXP__DEP_YTD_SBS_VS_BUDs;
                        reportViewer.ViewerCore.LogOnInfo = new TableLogOnInfos();
                        reportViewer.ViewerCore.LogOnInfo.Add(GetODBCTableLogOnInfo("Mori", "DESKTOP-SPSH5MS\\MSSQLSERVER2014", "Nav_Dwh", "sa", "retail_ps2010"));
                        break;
                    case "ExpDepSbs":
                        EXP_DEP_SBS EXP_DEP_SBSs = new EXP_DEP_SBS();
                        EXP_DEP_SBSs.Load("Reports/EXP DEP SBS.rpt");
                        EXP_DEP_SBSs.SetParameterValue("@dbName", "Mori");
                        EXP_DEP_SBSs.SetParameterValue("@ST_Date", dateFrom.SelectedDate.Value);
                        EXP_DEP_SBSs.SetParameterValue("@End_Date", dateTo.SelectedDate.Value);
                        foreach (var companyName in CompanyNames)
                            SelectedCompanies += "[" + companyName + "]";
                        foreach (var costCenterName in CostCenterNames)
                            SelectedCostCenters += "[" + costCenterName + "]";
                        EXP_DEP_SBSs.SetParameterValue("@SelectedCompanies", SelectedCompanies);
                        EXP_DEP_SBSs.SetParameterValue("@CostCenter", SelectedCostCenters);
                        EXP_DEP_SBSs.SetParameterValue("@UserCode", 7418);
                        EXP_DEP_SBSs.SetParameterValue("@ReportType", 1);
                        EXP_DEP_SBSs.SetDatabaseLogon("sa", "P@ssw0rd");
                        reportViewer.ViewerCore.ReportSource = EXP_DEP_SBSs;
                        reportViewer.ViewerCore.LogOnInfo = new TableLogOnInfos();
                        reportViewer.ViewerCore.LogOnInfo.Add(GetODBCTableLogOnInfo("Mori", "DESKTOP-SPSH5MS\\MSSQLSERVER2014", "Nav_Dwh", "sa", "retail_ps2010"));
                        break;
                    case "BsTamaraDescri":
                        BS_TAMARA_DESCRI BS_TAMARA_DESCRIs = new BS_TAMARA_DESCRI();
                        BS_TAMARA_DESCRIs.Load("Reports/BS_TAMARA_DESCRI.rpt");
                        BS_TAMARA_DESCRIs.SetParameterValue("@dbName", "Mori");
                        BS_TAMARA_DESCRIs.SetParameterValue("@ST_Date", dateFrom.SelectedDate.Value);
                        BS_TAMARA_DESCRIs.SetParameterValue("@End_Date", dateTo.SelectedDate.Value);
                        foreach (var companyName in CompanyNames)
                            SelectedCompanies += "[" + companyName + "]";
                        foreach (var costCenterName in CostCenterNames)
                            SelectedCostCenters += "[" + costCenterName + "]";
                        BS_TAMARA_DESCRIs.SetParameterValue("@SelectedCompanies", SelectedCompanies);
                        BS_TAMARA_DESCRIs.SetParameterValue("@CostCenter", SelectedCostCenters);
                        BS_TAMARA_DESCRIs.SetParameterValue("@UserCode", 7418);
                        BS_TAMARA_DESCRIs.SetParameterValue("@ReportType", 7);
                        BS_TAMARA_DESCRIs.SetDatabaseLogon("sa", "P@ssw0rd");
                        reportViewer.ViewerCore.ReportSource = BS_TAMARA_DESCRIs;
                        reportViewer.ViewerCore.LogOnInfo = new TableLogOnInfos();
                        reportViewer.ViewerCore.LogOnInfo.Add(GetODBCTableLogOnInfo("Mori", "DESKTOP-SPSH5MS\\MSSQLSERVER2014", "Nav_Dwh", "sa", "retail_ps2010"));
                        break;
                    case "PlVsBud":
                        PL_VS_BUD PL_VS__BUDs = new PL_VS_BUD();
                        PL_VS__BUDs.Load("Reports/PL_VS_BUD.rpt");
                        PL_VS__BUDs.SetParameterValue("@dbName", "Mori");
                        PL_VS__BUDs.SetParameterValue("@ST_Date", dateFrom.SelectedDate.Value);
                        PL_VS__BUDs.SetParameterValue("@End_Date", dateTo.SelectedDate.Value);
                        foreach (var companyName in CompanyNames)
                            SelectedCompanies += "[" + companyName + "]";
                        foreach (var costCenterName in CostCenterNames)
                            SelectedCostCenters += "[" + costCenterName + "]";
                        PL_VS__BUDs.SetParameterValue("@SelectedCompanies", SelectedCompanies);
                        PL_VS__BUDs.SetParameterValue("@CostCenter", SelectedCostCenters);
                        PL_VS__BUDs.SetParameterValue("@UserCode", 7418);
                        PL_VS__BUDs.SetParameterValue("@ReportType", 6);
                        PL_VS__BUDs.SetDatabaseLogon("sa", "P@ssw0rd");
                        reportViewer.ViewerCore.ReportSource = PL_VS__BUDs;
                        reportViewer.ViewerCore.LogOnInfo = new TableLogOnInfos();
                        reportViewer.ViewerCore.LogOnInfo.Add(GetODBCTableLogOnInfo("Mori", "DESKTOP-SPSH5MS\\MSSQLSERVER2014", "Nav_Dwh", "sa", "retail_ps2010"));
                        break;
                    default:
                        break;
                }
            }
        }

        private void Company_Checked(object sender, RoutedEventArgs e)
        {
            var Sender = (CheckBox)sender;

            if ((bool)Sender.IsChecked)
                CompanyNames.Add(Sender.Content.ToString());
            else
                CompanyNames.Remove(Sender.Content.ToString());
        }

        private void CostCenter_Checked(object sender, RoutedEventArgs e)
        {
            var Sender = (CheckBox)sender;

            if ((bool)Sender.IsChecked)
                CostCenterNames.Add(Sender.Content.ToString());
            else
                CostCenterNames.Remove(Sender.Content.ToString());
        }

        #region CheckListLoading        
        private ObservableCollection<Company> _companyList;
        public ObservableCollection<Company> CompanyList
        {
            get { return _companyList; }
            set { _companyList = value; }
        }

        private ObservableCollection<CostCenter> _costCenterList;
        public ObservableCollection<CostCenter> CostCenterList
        {
            get { return _costCenterList; }
            set { _costCenterList = value; }
        }

        public void LoadCompanies()
        {
            CompanyList = new ObservableCollection<Company>();

            CompanyList.Add(new Company { CompanyName = "101-Mori Co_" });
            CompanyList.Add(new Company { CompanyName = "102-Mori Sushi" });
            CompanyList.Add(new Company { CompanyName = "103-Mori Yaki" });
            CompanyList.Add(new Company { CompanyName = "111-Al Tamara" });
            CompanyList.Add(new Company { CompanyName = "112-Kitchen" });
            CompanyList.Add(new Company { CompanyName = "113-Zeitouna" });
            CompanyList.Add(new Company { CompanyName = "121-Teds" });
            CompanyList.Add(new Company { CompanyName = "122-Feed" });
            CompanyList.Add(new Company { CompanyName = "131-Kozmo" });
        }
        public void LoadCostCenters()
        {
            CostCenterList = new ObservableCollection<CostCenter>();

            CostCenterList.Add(new CostCenter { DimensionCode = "COST CENTER", CostCenterCode = "1", CostCenterName = "GENERAL ADMIN" });
            CostCenterList.Add(new CostCenter { DimensionCode = "COST CENTER", CostCenterCode = "2", CostCenterName = "ADMIN&DUTY HO" });
            CostCenterList.Add(new CostCenter { DimensionCode = "COST CENTER", CostCenterCode = "3", CostCenterName = "TOP MANAGEMENT" });
            CostCenterList.Add(new CostCenter { DimensionCode = "COST CENTER", CostCenterCode = "4", CostCenterName = "FINANCE " });
            CostCenterList.Add(new CostCenter { DimensionCode = "COST CENTER", CostCenterCode = "5", CostCenterName = "PURCHASING" });
            CostCenterList.Add(new CostCenter { DimensionCode = "COST CENTER", CostCenterCode = "6", CostCenterName = "COST CONTROLL" });
            CostCenterList.Add(new CostCenter { DimensionCode = "COST CENTER", CostCenterCode = "7", CostCenterName = "COLD WARHOUSE" });
            CostCenterList.Add(new CostCenter { DimensionCode = "COST CENTER", CostCenterCode = "8", CostCenterName = "DRY WARHOUSE" });
            CostCenterList.Add(new CostCenter { DimensionCode = "COST CENTER", CostCenterCode = "9", CostCenterName = "FIXEDCASSETS WARHOUSE" });
            CostCenterList.Add(new CostCenter { DimensionCode = "COST CENTER", CostCenterCode = "10", CostCenterName = "INTERNAL AUDIT" });
            CostCenterList.Add(new CostCenter { DimensionCode = "COST CENTER", CostCenterCode = "11", CostCenterName = "H R" });
            CostCenterList.Add(new CostCenter { DimensionCode = "COST CENTER", CostCenterCode = "12", CostCenterName = "OPERATION MANAGEMENT" });
            CostCenterList.Add(new CostCenter { DimensionCode = "COST CENTER", CostCenterCode = "13", CostCenterName = "OPERATION AREA MANAGERS" });
            CostCenterList.Add(new CostCenter { DimensionCode = "COST CENTER", CostCenterCode = "14", CostCenterName = "AXECTIVE CHEEFS" });
            CostCenterList.Add(new CostCenter { DimensionCode = "COST CENTER", CostCenterCode = "15", CostCenterName = "CATRING  MANAGEMENT" });
            CostCenterList.Add(new CostCenter { DimensionCode = "COST CENTER", CostCenterCode = "16", CostCenterName = "QUALITY CONTROL" });
            CostCenterList.Add(new CostCenter { DimensionCode = "COST CENTER", CostCenterCode = "17", CostCenterName = "CALL CENTER" });
            CostCenterList.Add(new CostCenter { DimensionCode = "COST CENTER", CostCenterCode = "18", CostCenterName = "I T " });
            CostCenterList.Add(new CostCenter { DimensionCode = "COST CENTER", CostCenterCode = "19", CostCenterName = "ENGNEERING" });
            CostCenterList.Add(new CostCenter { DimensionCode = "COST CENTER", CostCenterCode = "20", CostCenterName = "MARKTING" });
            CostCenterList.Add(new CostCenter { DimensionCode = "COST CENTER", CostCenterCode = "21", CostCenterName = "LEGAL" });
            CostCenterList.Add(new CostCenter { DimensionCode = "COST CENTER", CostCenterCode = "22", CostCenterName = "SECRTARY" });
            CostCenterList.Add(new CostCenter { DimensionCode = "COST CENTER", CostCenterCode = "23", CostCenterName = "FLEET" });
            CostCenterList.Add(new CostCenter { DimensionCode = "COST CENTER", CostCenterCode = "24", CostCenterName = "FRANCHIZE" });
            CostCenterList.Add(new CostCenter { DimensionCode = "COST CENTER", CostCenterCode = "25", CostCenterName = "TRANING" });
            CostCenterList.Add(new CostCenter { DimensionCode = "COST CENTER", CostCenterCode = "26", CostCenterName = "DEVLOPMENT" });
            CostCenterList.Add(new CostCenter { DimensionCode = "COST CENTER", CostCenterCode = "27", CostCenterName = "CUSTMER SERVICE" });
            CostCenterList.Add(new CostCenter { DimensionCode = "COST CENTER", CostCenterCode = "28", CostCenterName = "FISH  CK" });
            CostCenterList.Add(new CostCenter { DimensionCode = "COST CENTER", CostCenterCode = "29", CostCenterName = "BUTCHER CK" });
            CostCenterList.Add(new CostCenter { DimensionCode = "COST CENTER", CostCenterCode = "30", CostCenterName = "SAUCE CK" });
            CostCenterList.Add(new CostCenter { DimensionCode = "COST CENTER", CostCenterCode = "31", CostCenterName = "BAKERY & PASTSRY" });
            CostCenterList.Add(new CostCenter { DimensionCode = "COST CENTER", CostCenterCode = "32", CostCenterName = "ADMIN & DUTY CK" });
            CostCenterList.Add(new CostCenter { DimensionCode = "COST CENTER", CostCenterCode = "33", CostCenterName = "QUALITY CONTROL CK" });
            CostCenterList.Add(new CostCenter { DimensionCode = "COST CENTER", CostCenterCode = "34", CostCenterName = "STEWARD CK" });
            CostCenterList.Add(new CostCenter { DimensionCode = "COST CENTER", CostCenterCode = "35", CostCenterName = "ALEX WARHOUSE" });
            CostCenterList.Add(new CostCenter { DimensionCode = "COST CENTER", CostCenterCode = "36", CostCenterName = "WAREHOUSE" });
            CostCenterList.Add(new CostCenter { DimensionCode = "COST CENTER", CostCenterCode = "37", CostCenterName = "OPERATION" });
            CostCenterList.Add(new CostCenter { DimensionCode = "COST CENTER", CostCenterCode = "38", CostCenterName = "CENTRAL KITCHEN TAMARA" });
            CostCenterList.Add(new CostCenter { DimensionCode = "COST CENTER", CostCenterCode = "39", CostCenterName = "QUALITY" });
            CostCenterList.Add(new CostCenter { DimensionCode = "COST CENTER", CostCenterCode = "40", CostCenterName = "HR- ADMIN HEAD OFFICE" });
            CostCenterList.Add(new CostCenter { DimensionCode = "COST CENTER", CostCenterCode = "41", CostCenterName = "HR - CALL CENTER" });
            CostCenterList.Add(new CostCenter { DimensionCode = "COST CENTER", CostCenterCode = "42", CostCenterName = "HR - BUFFET" });
            CostCenterList.Add(new CostCenter { DimensionCode = "COST CENTER", CostCenterCode = "43", CostCenterName = "HR - FLEET" });
            CostCenterList.Add(new CostCenter { DimensionCode = "COST CENTER", CostCenterCode = "44", CostCenterName = "HR - TRAINING" });
            CostCenterList.Add(new CostCenter { DimensionCode = "COST CENTER", CostCenterCode = "45", CostCenterName = "PRODUCTION" });
            CostCenterList.Add(new CostCenter { DimensionCode = "COST CENTER", CostCenterCode = "5050", CostCenterName = "HEAD QUARTER-GENERAL" });
            CostCenterList.Add(new CostCenter { DimensionCode = "COST CENTER", CostCenterCode = "COSMO", CostCenterName = "COSMO, BEGIN" });
            CostCenterList.Add(new CostCenter { DimensionCode = "COST CENTER", CostCenterCode = "COSMO-025", CostCenterName = "COSMO -ZAMALEK" });
            CostCenterList.Add(new CostCenter { DimensionCode = "COST CENTER", CostCenterCode = "COSMO-026", CostCenterName = "COSMO -CATRING" });
            CostCenterList.Add(new CostCenter { DimensionCode = "COST CENTER", CostCenterCode = "COSMO-027", CostCenterName = "COSMO -HI CENDA WHITE" });
            CostCenterList.Add(new CostCenter { DimensionCode = "COST CENTER", CostCenterCode = "COSMO-028", CostCenterName = "COSMO -HI CENDA BAY" });
            CostCenterList.Add(new CostCenter { DimensionCode = "COST CENTER", CostCenterCode = "COSMO-029", CostCenterName = "COSMO -MAADI" });
            CostCenterList.Add(new CostCenter { DimensionCode = "COST CENTER", CostCenterCode = "COSMO-030", CostCenterName = "COSMO -MARASSI" });
            CostCenterList.Add(new CostCenter { DimensionCode = "COST CENTER", CostCenterCode = "COSMO-031", CostCenterName = "COSMO -C F C" });
            CostCenterList.Add(new CostCenter { DimensionCode = "COST CENTER", CostCenterCode = "COSMO-032", CostCenterName = "COSMO -MALL OF EGYPT" });
            CostCenterList.Add(new CostCenter { DimensionCode = "COST CENTER", CostCenterCode = "COSMO-033", CostCenterName = "COSMO -DISIGNIA" });
            CostCenterList.Add(new CostCenter { DimensionCode = "COST CENTER", CostCenterCode = "COSMO-034", CostCenterName = "COSMO -HELIOPOLIS" });
            CostCenterList.Add(new CostCenter { DimensionCode = "COST CENTER", CostCenterCode = "COSMO-035", CostCenterName = "COSMO-ALEX" });
            CostCenterList.Add(new CostCenter { DimensionCode = "COST CENTER", CostCenterCode = "COSMO-036", CostCenterName = "COSMO - ARKAN" });
            CostCenterList.Add(new CostCenter { DimensionCode = "COST CENTER", CostCenterCode = "COSMO-037", CostCenterName = "COSMO-WATER WAY" });
            CostCenterList.Add(new CostCenter { DimensionCode = "COST CENTER", CostCenterCode = "COSMO-038", CostCenterName = "COSMO - POINT  90" });
            CostCenterList.Add(new CostCenter { DimensionCode = "COST CENTER", CostCenterCode = "COSMO-039", CostCenterName = "CAPITAL BUSINESS" });
            CostCenterList.Add(new CostCenter { DimensionCode = "COST CENTER", CostCenterCode = "COSMO-040", CostCenterName = "CITY STARS" });
            CostCenterList.Add(new CostCenter { DimensionCode = "COST CENTER", CostCenterCode = "COSMO-041", CostCenterName = "COSMO -MARASSI 2" });
            CostCenterList.Add(new CostCenter { DimensionCode = "COST CENTER", CostCenterCode = "COSMO-042", CostCenterName = "COSMO LA VISTA" });
            CostCenterList.Add(new CostCenter { DimensionCode = "COST CENTER", CostCenterCode = "COSMO-043", CostCenterName = "COSMO DOPLPMASEEN" });
            CostCenterList.Add(new CostCenter { DimensionCode = "COST CENTER", CostCenterCode = "COSMO-999", CostCenterName = "COSMO, END" });
            CostCenterList.Add(new CostCenter { DimensionCode = "COST CENTER", CostCenterCode = "FEED", CostCenterName = "FEED, BEGIN" });
            CostCenterList.Add(new CostCenter { DimensionCode = "COST CENTER", CostCenterCode = "FEED-025", CostCenterName = "POINT 90" });
            CostCenterList.Add(new CostCenter { DimensionCode = "COST CENTER", CostCenterCode = "FEED-026", CostCenterName = "DIZINA" });
            CostCenterList.Add(new CostCenter { DimensionCode = "COST CENTER", CostCenterCode = "FEED-027", CostCenterName = "MARASSI" });
            CostCenterList.Add(new CostCenter { DimensionCode = "COST CENTER", CostCenterCode = "FEED-028", CostCenterName = "HACIENDA BAY" });
            CostCenterList.Add(new CostCenter { DimensionCode = "COST CENTER", CostCenterCode = "FEED-029", CostCenterName = "CAPITAL BUSINESS" });
            CostCenterList.Add(new CostCenter { DimensionCode = "COST CENTER", CostCenterCode = "FEED-030", CostCenterName = "CATRING" });
            CostCenterList.Add(new CostCenter { DimensionCode = "COST CENTER", CostCenterCode = "FEED-999", CostCenterName = "FEED, END" });
            CostCenterList.Add(new CostCenter { DimensionCode = "COST CENTER", CostCenterCode = "KITCHEN", CostCenterName = "KITCHEN, BEGIN" });
            CostCenterList.Add(new CostCenter { DimensionCode = "COST CENTER", CostCenterCode = "KITCHEN-025", CostCenterName = "CITY STARS" });
            CostCenterList.Add(new CostCenter { DimensionCode = "COST CENTER", CostCenterCode = "KITCHEN-999", CostCenterName = "KITCHEN, END" });
            CostCenterList.Add(new CostCenter { DimensionCode = "COST CENTER", CostCenterCode = "SUSHI", CostCenterName = "SUSHI, BEGIN" });
            CostCenterList.Add(new CostCenter { DimensionCode = "COST CENTER", CostCenterCode = "SUSHI-025", CostCenterName = "MOHANDSEEN" });
            CostCenterList.Add(new CostCenter { DimensionCode = "COST CENTER", CostCenterCode = "SUSHI-026", CostCenterName = "ZAMALEK" });
            CostCenterList.Add(new CostCenter { DimensionCode = "COST CENTER", CostCenterCode = "SUSHI-027", CostCenterName = "MAADI" });
            CostCenterList.Add(new CostCenter { DimensionCode = "COST CENTER", CostCenterCode = "SUSHI-028", CostCenterName = "TIVOLI" });
            CostCenterList.Add(new CostCenter { DimensionCode = "COST CENTER", CostCenterCode = "SUSHI-029", CostCenterName = "EL GOUNA" });
            CostCenterList.Add(new CostCenter { DimensionCode = "COST CENTER", CostCenterCode = "SUSHI-030", CostCenterName = "DOWN TAWN" });
            CostCenterList.Add(new CostCenter { DimensionCode = "COST CENTER", CostCenterCode = "SUSHI-031", CostCenterName = "CITY STARS" });
            CostCenterList.Add(new CostCenter { DimensionCode = "COST CENTER", CostCenterCode = "SUSHI-032", CostCenterName = "DEPLOMASIEEN" });
            CostCenterList.Add(new CostCenter { DimensionCode = "COST CENTER", CostCenterCode = "SUSHI-033", CostCenterName = "MARINA PLAT FORM" });
            CostCenterList.Add(new CostCenter { DimensionCode = "COST CENTER", CostCenterCode = "SUSHI-034", CostCenterName = "C F C" });
            CostCenterList.Add(new CostCenter { DimensionCode = "COST CENTER", CostCenterCode = "SUSHI-035", CostCenterName = "WATER WAY" });
            CostCenterList.Add(new CostCenter { DimensionCode = "COST CENTER", CostCenterCode = "SUSHI-036", CostCenterName = "POINT  90" });
            CostCenterList.Add(new CostCenter { DimensionCode = "COST CENTER", CostCenterCode = "SUSHI-037", CostCenterName = "MARSY" });
            CostCenterList.Add(new CostCenter { DimensionCode = "COST CENTER", CostCenterCode = "SUSHI-038", CostCenterName = "DESIGENIA" });
            CostCenterList.Add(new CostCenter { DimensionCode = "COST CENTER", CostCenterCode = "SUSHI-039", CostCenterName = "MORI ALEX" });
            CostCenterList.Add(new CostCenter { DimensionCode = "COST CENTER", CostCenterCode = "SUSHI-040", CostCenterName = "CATRING" });
            CostCenterList.Add(new CostCenter { DimensionCode = "COST CENTER", CostCenterCode = "SUSHI-041", CostCenterName = "CAPITAL BUSINESS" });
            CostCenterList.Add(new CostCenter { DimensionCode = "COST CENTER", CostCenterCode = "SUSHI-042", CostCenterName = "EL YAKHT" });
            CostCenterList.Add(new CostCenter { DimensionCode = "COST CENTER", CostCenterCode = "SUSHI-043", CostCenterName = "Sushi - Noua " });
            CostCenterList.Add(new CostCenter { DimensionCode = "COST CENTER", CostCenterCode = "SUSHI-044", CostCenterName = "SUSHI LA VISTA" });
            CostCenterList.Add(new CostCenter { DimensionCode = "COST CENTER", CostCenterCode = "SUSHI-999", CostCenterName = "SUSHI,END" });
            CostCenterList.Add(new CostCenter { DimensionCode = "COST CENTER", CostCenterCode = "TAMARA", CostCenterName = "TAMARA, BEGIN" });
            CostCenterList.Add(new CostCenter { DimensionCode = "COST CENTER", CostCenterCode = "TAMARA-025", CostCenterName = "CATRING" });
            CostCenterList.Add(new CostCenter { DimensionCode = "COST CENTER", CostCenterCode = "TAMARA-026", CostCenterName = "CFC" });
            CostCenterList.Add(new CostCenter { DimensionCode = "COST CENTER", CostCenterCode = "TAMARA-027", CostCenterName = "WATERWAY" });
            CostCenterList.Add(new CostCenter { DimensionCode = "COST CENTER", CostCenterCode = "TAMARA-028", CostCenterName = "POINT 90" });
            CostCenterList.Add(new CostCenter { DimensionCode = "COST CENTER", CostCenterCode = "TAMARA-029", CostCenterName = "MARASSI" });
            CostCenterList.Add(new CostCenter { DimensionCode = "COST CENTER", CostCenterCode = "TAMARA-030", CostCenterName = "HACIENDA BAY" });
            CostCenterList.Add(new CostCenter { DimensionCode = "COST CENTER", CostCenterCode = "TAMARA-031", CostCenterName = "DISEGNIA" });
            CostCenterList.Add(new CostCenter { DimensionCode = "COST CENTER", CostCenterCode = "TAMARA-032", CostCenterName = "ELYAKHT" });
            CostCenterList.Add(new CostCenter { DimensionCode = "COST CENTER", CostCenterCode = "TAMARA-033", CostCenterName = "CAPITAL" });
            CostCenterList.Add(new CostCenter { DimensionCode = "COST CENTER", CostCenterCode = "TAMARA-034", CostCenterName = "MALL OF EGYPT" });
            CostCenterList.Add(new CostCenter { DimensionCode = "COST CENTER", CostCenterCode = "TAMARA-035", CostCenterName = "ELNA MARASSI" });
            CostCenterList.Add(new CostCenter { DimensionCode = "COST CENTER", CostCenterCode = "TAMARA-036", CostCenterName = "" });
            CostCenterList.Add(new CostCenter { DimensionCode = "COST CENTER", CostCenterCode = "TAMARA-037", CostCenterName = "EL AGOZA" });
            CostCenterList.Add(new CostCenter { DimensionCode = "COST CENTER", CostCenterCode = "TAMARA-999", CostCenterName = "TAMARA, END" });
            CostCenterList.Add(new CostCenter { DimensionCode = "COST CENTER", CostCenterCode = "TEDS", CostCenterName = "TEDS, BEGIN" });
            CostCenterList.Add(new CostCenter { DimensionCode = "COST CENTER", CostCenterCode = "TEDS-025", CostCenterName = "CITY STARS" });
            CostCenterList.Add(new CostCenter { DimensionCode = "COST CENTER", CostCenterCode = "TEDS-999", CostCenterName = "TEDS, END" });
            CostCenterList.Add(new CostCenter { DimensionCode = "COST CENTER", CostCenterCode = "YAKI", CostCenterName = "YAKI, BEGIN" });
            CostCenterList.Add(new CostCenter { DimensionCode = "COST CENTER", CostCenterCode = "YAKI-025", CostCenterName = "DANDY MALL" });
            CostCenterList.Add(new CostCenter { DimensionCode = "COST CENTER", CostCenterCode = "YAKI-026", CostCenterName = "MALL EL ARAB" });
            CostCenterList.Add(new CostCenter { DimensionCode = "COST CENTER", CostCenterCode = "YAKI-999", CostCenterName = "YAKI, END" });
            CostCenterList.Add(new CostCenter { DimensionCode = "COST CENTER", CostCenterCode = "ZAYTUNA", CostCenterName = "ZAYTUNA, BEGIN" });
            CostCenterList.Add(new CostCenter { DimensionCode = "COST CENTER", CostCenterCode = "ZAYTUNA-025", CostCenterName = "ZAYED" });
            CostCenterList.Add(new CostCenter { DimensionCode = "COST CENTER", CostCenterCode = "ZAYTUNA-026", CostCenterName = "CAPITAL BUSNISS" });
            CostCenterList.Add(new CostCenter { DimensionCode = "COST CENTER", CostCenterCode = "ZAYTUNA-999", CostCenterName = "ZAYTUNA, END" });
        }
        #endregion
    }
}
