﻿namespace Nav_Solution.UI.Temp
{
    public class Company
    {
        public string CompanyName { get; set; }
    }

    public class CostCenter
    {
        public string DimensionCode { get; set; }
        public string CostCenterCode { get; set; }
        public string CostCenterName { get; set; }
    }
}