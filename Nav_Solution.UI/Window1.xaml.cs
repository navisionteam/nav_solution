﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Nav_Solution.DataAccess;
using Nav_Solution.DataAccess.Repositories;
using Nav_Solution.DomainModel.Models;

namespace Nav_Solution.UI
{
    /// <summary>
    /// Interaction logic for Window1.xaml
    /// </summary>
    /// 
    public class TestClass
    {
        public string id  { get; set; }
        public string name { get; set; }
    }

    public partial class Window1 : Window
    {

        NavSolutionEntities db = new NavSolutionEntities();
        public Window1()
        {
            InitializeComponent();
            loaddata("");
        }

        public void loaddata(string search)
        {
            try
            {
                TestData = new ObservableCollection<TestClass>();
                if (search == "")
                {
                    var datas = db.tbl_Main_account.ToList();
                    foreach (var data in datas)
                    {
                        TestData.Add(new TestClass { id = data.id.ToString(), name = data.name });
                    }
                    Datagrid1.ItemsSource = TestData;
                }
                else
                {
                    var datas = db.tbl_Main_account.Where(p => p.name.Contains(search) || p.id.ToString() == search || p.name_Eng.Contains(search)).ToList();
                    foreach (var data in datas)
                    {
                        TestData.Add(new TestClass { id = data.id.ToString(), name = data.name });
                    }
                    Datagrid1.ItemsSource = TestData;
                }
            }
            catch { }
        }

        private ObservableCollection<TestClass> _testData = new ObservableCollection<TestClass>();
        public ObservableCollection<TestClass> TestData
        {
            get { return _testData; }
            set { _testData = value; }
        }
        string selectedid;
        private void DataGridCell_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            try
            {
                var dataGridCellTarget = (DataGridRow)sender;
                var id = ((TestClass)dataGridCellTarget.DataContext).id;
                selectedid = id;
                ContentView1.Content = new AccountSetup(id);
                var mainaccount = db.tbl_Main_account.Find(int.Parse(id));
                Name.Text = mainaccount.name;
                Order.Text = mainaccount.Order.ToString();
                BS.IsChecked =true;
                PL.IsChecked = mainaccount.PL;
                //var Formula = db.tbl_Account_Formulas.Where(p => p.Main_Account_id.ToString() == ds[0]);
            }
            catch { }
        }

        private void SearchBox_TextChanged(object sender, TextChangedEventArgs e)
        {
            loaddata(((TextBox)sender).Text);
        }

        private void Delete_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                db = new NavSolutionEntities();
                var mainaccount = db.tbl_Main_account.Find(int.Parse(selectedid));
                db.tbl_Account_Formulas.RemoveRange(mainaccount.tbl_Account_Formulas);
                db.SaveChanges();
                db.tbl_related_Acc.RemoveRange(mainaccount.tbl_related_Acc);
                db.SaveChanges();
                db.tbl_Main_account.Remove(mainaccount);
                db.SaveChanges();
                Name.Text = "";
                Order.Text = "";
                PL.IsChecked = false;
                selectedid = "";
                loaddata("");
            }
            catch
            { }
        }

        private void Save_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (selectedid == "")
                {
                    var mainaccount = new tbl_Main_account();
                    mainaccount.name = Name.Text;
                    mainaccount.Order = int.Parse(Order.Text);
                    mainaccount.PL = PL.IsChecked.Value;
                    db.tbl_Main_account.Add(mainaccount);
                    db.SaveChanges();
                    Name.Text = "";
                    Order.Text = "";
                    PL.IsChecked = false;
                    selectedid = "";
                    loaddata("");
                }
                else
                {
                    var mainaccount = db.tbl_Main_account.Find(int.Parse(selectedid));
                    mainaccount.name = Name.Text;
                    mainaccount.Order = int.Parse(Order.Text);
                    mainaccount.BS = BS.IsChecked.Value;
                    db.SaveChanges();
                    mainaccount = db.tbl_Main_account.Find(int.Parse(selectedid));
                    Name.Text = mainaccount.name;
                    Order.Text = mainaccount.Order.ToString();
                    BS.IsChecked = mainaccount.BS;
                    PL.IsChecked = mainaccount.PL;
                    Name.Text = "";
                    Order.Text = "";
                    PL.IsChecked = false;
                    selectedid = "";
                    loaddata("");
                }
            }
            catch { }
        }

        private void New_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                Name.Text = "";
                Order.Text = "";
                PL.IsChecked = false;
                selectedid = "";
                loaddata("");
            }
            catch { }
        }
    }
}
