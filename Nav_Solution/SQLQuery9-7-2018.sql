CREATE TABLE [dbo].[tbl_Account_Formulas](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[Main_Account_id] [int] NULL,
	[Formula] [nvarchar](max) NULL,
 CONSTRAINT [PK_tbl_Account_Formulas] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[tbl_Main_account]    Script Date: 09/07/2018 1:49:47 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbl_Main_account](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[name] [nvarchar](max) NULL,
	[name_Eng] [nvarchar](max) NULL,
 CONSTRAINT [PK_tbl_Main_account] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[tbl_related_Acc]    Script Date: 09/07/2018 1:49:47 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbl_related_Acc](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[MainAccountId] [int] NULL,
	[AccountNum] [nvarchar](max) NULL,
 CONSTRAINT [PK_tbl_related_Acc] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[tbl_related_Loc]    Script Date: 09/07/2018 1:49:47 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbl_related_Loc](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[Locationname] [nvarchar](max) NULL,
 CONSTRAINT [PK_tbl_related_Loc] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[tbl_Users]    Script Date: 09/07/2018 1:49:47 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbl_Users](
	[id] [int] NOT NULL,
	[UserName] [nvarchar](max) NULL,
	[Password] [nvarchar](max) NULL,
	[PermissionSet] [int] NULL,
 CONSTRAINT [PK_tbl_Users] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[template]    Script Date: 09/07/2018 1:49:47 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[template](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[name] [nvarchar](max) NULL,
 CONSTRAINT [PK_template] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
ALTER TABLE [dbo].[tbl_Account_Formulas]  WITH CHECK ADD  CONSTRAINT [FK_tbl_Account_Formulas_tbl_Main_account] FOREIGN KEY([Main_Account_id])
REFERENCES [dbo].[tbl_Main_account] ([id])
GO
ALTER TABLE [dbo].[tbl_Account_Formulas] CHECK CONSTRAINT [FK_tbl_Account_Formulas_tbl_Main_account]
GO
ALTER TABLE [dbo].[tbl_related_Acc]  WITH CHECK ADD  CONSTRAINT [FK_tbl_related_Acc_tbl_Main_account] FOREIGN KEY([MainAccountId])
REFERENCES [dbo].[tbl_Main_account] ([id])
GO
ALTER TABLE [dbo].[tbl_related_Acc] CHECK CONSTRAINT [FK_tbl_related_Acc_tbl_Main_account]
GO